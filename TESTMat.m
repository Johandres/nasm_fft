s = 64;
X=dftmtx(s);
for c = 1:s
    Y(c)=sin((2*pi*(c))/(s));
    for r = 1:s
        Xr(r,c)=real(X(r,c));
        Xi(r,c)=(imag(X(r,c)));
    end
end
x1 = 0:pi/100:2*pi;
y1= (2*pi)/(s):(2*pi)/(s):2*pi;
subplot(2,1,1)
plot(y1,Y,'ro',x1,sin(x1),'b','markersize',4,'markeredgecolor','r','markerfacecolor','r')
xlabel('t')
ylabel('x')
title('f(t)')

xlabel('t')
ylabel('x')
title('f(t)')
fs=1;
subplot(2,1,2)
wc=pi*fs; %frecuencia l�mite de Nyquist
ww=-pi:(pi/64):pi;
wwt=0:(2*pi/128):2*pi;
Fw=fft(sin(wwt));
gw=fftshift(Fw);

 %transformada r�pida de Fourier
  y=fft(Y);
  g=fftshift(y);
  w=-pi: 2*(pi)/(s-1):pi;
  plot(ww,abs(gw),'b',w,abs(g),'--','markersize',3,'markeredgecolor','r','markerfacecolor','r')
  set(gca,'XTick',-pi:pi/2:pi)
  set(gca,'XTickLabel',{'-\pi','-1/2\pi','0','1/2\pi','\pi'})
  xlim([-pi,pi])
  xlabel('\omega')
 ylabel('|F(w)|')
 title('Transformada Sin(x)')
  figure
 plot(ww,imag(gw),'b',w,imag(g),'--','markersize',3,'markeredgecolor','r','markerfacecolor','r')
 xlabel('\omega')
 ylabel('Imaginaria F(w)')
 title('Transformada Sin(x)')
 figure
 plot(ww,real(gw),'b',w,real(g),'--','markersize',3,'markeredgecolor','r','markerfacecolor','r')
 xlabel('\omega')
 ylabel('Real F(w)')
 title('Transformada Sin(x)')